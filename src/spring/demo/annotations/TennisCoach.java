package spring.demo.annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class TennisCoach implements Coach {

	@Autowired
	@Qualifier("happyFortuneService")
	FortuneService fortuneService;
	
	public TennisCoach (){
		
	}
	
	/*public void setFortuneService(FortuneService theFortuneService) {
		this.fortuneService = theFortuneService;
	}*/

	@Override
	public String getDailyWorkout() {
		
		return "Practice your back hand volley";
	}

	@Override
	public String getDailyFortune() {
		
		return fortuneService.getFortune();
	}

}
